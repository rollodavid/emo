﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchingProgram
{
    class Program
    {
        #region Variables
        //Random
        static Random nc = new Random();

        //Abeceda
        static Dictionary<int, char> abeceda = new Dictionary<int, char>()
        {
            {0, 'a'},
            {1, 'b'},
            {2, 'c'},
            {3, 'd'},
            {4, 'e'},
            {5, 'f'},
            {6, 'g'},
            {7, 'h'},
            {8, 'i'},
            {9, 'j'},
            {10, 'k'},
            {11, 'l'},
            {12, 'm'},
            {13, 'n'},
            {14, 'o'},
            {15, 'p'},
            {16, 'q'},
            {17, 'r'},
            {18, 's'},
            {19, 't'},
            {20, 'u'},
            {21, 'v'},
            {22, 'w'},
            {23, 'x'},
            {24, 'y'},
            {25, 'z'},
            {26, '('},
            {27, ')'}
        };

        //Base
        static int countOfPopulation = 10;
        static int tMax = 1000;

        //Selection
        static int nSelect = 2; //Vybírám n nejlepších (Select)

        //Crossover
        static int countOfkids = 15;
        static double pCross = 0.5;

        //Mutation
        static double pMut = 0.1;
        #endregion

        #region Declaration
        static List<string> population;
        static List<string> bestOfPopulation;
        static List<string> kids;
        #endregion

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();

                string target = "print(helloworld)";

                Console.Write($"Abeceda = A-Z + '(' + ')'\nZvolte target [def. print(helloworld)]: ");
                string new_target = Console.ReadLine();
                if (new_target != "") target = new_target;

                while (true)
                {
                    Console.Clear();
                    Console.WriteLine("\nVýsledek: " + Search(target));
                    Console.Write("-------------------\nStiskněte 't' pro zvolení parametrů\nStiskněte 'k' pro ukončení\nNebo stiskněte cokoli pro opakování...");
                    ConsoleKey z = Console.ReadKey().Key;
                    if (z == ConsoleKey.K) return;
                    else if (z == ConsoleKey.T) break;
                }
            }
        }

        static string Search(string target)
        {
            //Vytvoření počáteční populace
            population = CreatePopulation(target);

            //Zobrazení počáteční polupace
            ShowList(population, "Generation 1", target);

            for (int i = 0; i < tMax; i++)
            {
                //Selece vybere n (nSelect) nejlepších jedinců
                bestOfPopulation = Generic.Selection(population, target, nSelect);

                //Křížení proběhne s pravděpodobností pCross a probíhá mezi rodiči 0-1, 1-2, ..., n-0 (řešeno modulem)
                kids = Generic.Crossover(bestOfPopulation, target, countOfkids, pCross);

                //Mutace proběhne s pravděpodobností pMut
                population = Generic.Mutation(kids, target, pMut, abeceda);

                if (population.Contains(target))
                {
                    ShowList(population, $"Generation {i}", target);
                    return population[population.IndexOf(target)];
                }
            }

            ShowList(population, $"Populace {tMax}", target);

            return $"Při {tMax} krocích nebylo nic nalezeno!";
        }

        static List<string> CreatePopulation(string target)
        {
            List<string> populace = new List<string>();

            for (int i = 0; i < countOfPopulation; i++)
            {
                string individual = "";

                for (int j = 0; j < target.Length; j++)
                    individual += abeceda[nc.Next(abeceda.Count)]; //generují se čísla a na základě nich se přiřadí znak z abecedy

                populace.Add(individual);
            }

            return populace;
        }

        static void ShowList(List<string> ls, string name, string target)
        {
            Console.WriteLine($"-----------------------\n{name}:");
            foreach (string item in ls) Console.WriteLine(item + " - " + CountOfSameLetters(item, target));
        }

        static int CountOfSameLetters(string item, string target)
        {
            int res = 0;
            for (int i = 0; i < target.Length; i++) if (item[i] == target[i]) res++;
            return res;
        }
    }
}
