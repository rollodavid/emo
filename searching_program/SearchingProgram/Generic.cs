﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchingProgram
{
    class Generic
    {
        static Random nc = new Random();
        static List<string> kids = new List<string>();
        static string individual;

        public static List<string> Selection(List<string> populace, string target, int n)
        {
            //Select vybírá jedince s největším počtem stejný znaků na stejných pozicích

            //Vytvoření listu
            Dictionary<string, int> individuals = new Dictionary<string, int>();
            foreach (string item in populace) if (!individuals.ContainsKey(item)) individuals.Add(item, 0);

            //Spočítání fitness
            for (int i = 0; i < populace.Count; i++)
                for (int j = 0; j < target.Length; j++)
                    if (populace[i][j] == target[j])
                        individuals[populace[i]]++;

            //Zjištění n nejlepších
            List<string> bestIndividuals = new List<string>();
            foreach (string individualKey in individuals.Keys)
            {
                if (bestIndividuals.Count < n) { bestIndividuals.Add(individualKey); }
                else
                {
                    string smallestOneWithFitness = bestIndividuals[0];

                    //Nejmenší z nejlepších
                    foreach (string bestOne in bestIndividuals)
                        if (individuals[smallestOneWithFitness] > individuals[bestOne])
                            smallestOneWithFitness = bestOne;

                    //Má nejmenší z nejlepších menší fitness než individualKey?
                    if (individuals[smallestOneWithFitness] < individuals[individualKey])
                    {
                        bestIndividuals.Remove(smallestOneWithFitness);
                        bestIndividuals.Add(individualKey);
                    }
                }
            }

            //Návrat
            if (bestIndividuals.Count == 1) bestIndividuals.Add(bestIndividuals[0]);
            return bestIndividuals;
        }

        public static List<string> Crossover(List<string> parents, string target, int countOfKids, double pCross)
        {
            kids.Clear();

            //Vytvořím počet dětí podle countOfKids
            //V každém cyklu
            for (int i = 0; kids.Count < countOfKids; i++)
            {
                string kid = "";
                for (int j = 0; j < target.Length; j++)
                {
                    //Získá dítě znak podle rodiče 0?
                    if (nc.NextDouble() < pCross) kid += parents[i % parents.Count][j];
                    else kid += parents[(i + 1) % parents.Count][j];
                }
                kids.Add(kid);
            }

            return kids;
        }

        public static List<string> Mutation(List<string> kids, string target, double pMut, Dictionary<int, char> abeceda)
        {
            //S pravděpodobností pMut se znak náhodně vybere z abecedy, jinak zůstane stejný
            //Vždy se řeší dítě na indexu 0. Upravené dítě přidám na konc listu a první dítě (na indexu 0) smažu.

            for (int i = 0; i < kids.Count; i++)
            {
                individual = "";
                for (int j = 0; j < target.Length; j++)
                {
                    //Proběhne mutace?
                    if (nc.NextDouble() < pMut) individual += abeceda[nc.Next(abeceda.Count)];
                    else individual += kids[0][j];
                }
                kids.RemoveAt(0);
                kids.Add(individual);
            }
            return kids;
        }
    }
}
