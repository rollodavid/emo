﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tabu_search_algorithm
{
    class Program
    {
        static Random nc = new Random();
        static int k = 10;
        static int n = 2;

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();

                int t_max = 2000;
                int s = 5;

                Console.Write($"f(x,y) = x^2 + y^2\nn = {n}\nk = {k}\nZvolte t_max (def. 2000): ");
                if (int.TryParse(Console.ReadLine(), out int i)) t_max = i;
                Console.Write("Zvolte s - velikost množiny zakázaných operací < n*k (20) (def. 5): ");
                if (int.TryParse(Console.ReadLine(), out int j)) s = j;

                if (s > n * k)
                {
                    Console.WriteLine(" --- 's' musí být menší než n*k (20) ---");
                    Console.ReadLine();
                    continue;
                }

                while (true)
                {
                    Console.Clear();
                    Start_Tabu_Search(t_max, s);
                    Console.Write("Stiskněte 't' pro zvolení parametrů\nStiskněte 'k' pro ukončení\nNebo stiskněte cokoli pro opakování...");
                    ConsoleKey z = Console.ReadKey().Key;
                    if (z == ConsoleKey.K) return;
                    else if (z == ConsoleKey.T) break;
                }
            }
        }

        static void Start_Tabu_Search(int t_max, int s)
        {
            string a = GenerateA(n, k);
            double f_fin = double.MaxValue;
            List<int> T = new List<int>();

            string a_fin;
            
            int S = k*n; //normálně se jedná o množinu transformací. V tomto případě jde o počet bitů, které se následně znegují

            for (int time = 1; time < t_max; time++) //Hlavní cyklus
            {
                double f_fin_loc = int.MaxValue;

                string a_best = "";
                int t_best = -1;

                for (int t = 0; t < S; t++)
                {
                    string a_new = Negace(a, t); // a':=ta;

                    double f1x = function1(a_new);
                    if (((!T.Contains(t)) && (f1x < f_fin_loc)) || (f1x < f_fin))
                    {
                        a_best = a_new;
                        t_best = t;
                        f_fin_loc = f1x;
                    }
                }

                if (f_fin_loc < f_fin)
                {
                    f_fin = f_fin_loc;
                    a_fin = a_best;

                    Console.WriteLine($"Nastavilo se nové minimum: (time={time})\na_fin: {a_fin}\nf_fin: {f_fin}\n----------------------");
                }

                a = a_best;

                if (T.Count < s) T.Add(t_best);
                else
                {
                    T.Add(t_best);
                    T.RemoveAt(0);
                }
            }
        }

        static string GenerateA(int n, int k)
        {
            string a = "";
            for (int i = 0; i < n * k; i++) a += nc.Next(0, 2);
            return a;
        }

        static double function1(string a)
        {
            List<int> parameters = new List<int>();
            for (int j = 0; j < n * k; j += k)
            {
                string a_n = "";
                for (int p = 0; p < k; p++) a_n += a[j + p];
                parameters.Add(GrayToInt(a_n)); //List int hodnot
            }

            int x = parameters[0];
            int y = parameters[1];

            //basic function
            return x * x + y * y;

            //rastrigen function
            //return (x * x - 10 * Math.Cos(2 * Math.PI * x)) + (y * y - 10 * Math.Cos(2 * Math.PI * y)) + 20;
        }

        static int GrayToInt(string a_n)
        {
            string binary = "";

            // MSB of binary code is same
            // as gray code
            binary += a_n[0];

            // Compute remaining bits
            for (int i = 1; i < a_n.Length; i++)
            {
                // If current bit is 0,
                // concatenate previous bit
                if (a_n[i] == '0') binary += binary[i - 1];

                // Else, concatenate invert of
                // previous bit
                else binary += (binary[i - 1] == '0' ? '1' : '0');
            }

            return Convert.ToInt32(binary, 2); ;
        }

        static string Negace(string a, int t)
        {
            string a_new = "";
            for (int j = 0; j < t; j++) a_new += a[j];
            if (a[t] == '1') a_new += "0";
            else a_new += "1";
            for (int j = t + 1; j < a.Length; j++) a_new += a[j];

            return a_new;
        }
    }
}
