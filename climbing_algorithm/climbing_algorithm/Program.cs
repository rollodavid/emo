﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace climbing_algorithm
{
    class Program
    {
        static Random nc = new Random();

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();

                int t_max = 100;
                int c_0 = 10;
                double p_mut = 0.05;

                Console.Write("f(x,y) = x^2 + y^2\nZvolte t_max (def. 100): ");
                if (int.TryParse(Console.ReadLine(), out int i)) t_max = i;
                Console.Write("Zvolte c - počet zmutovaných (def. 10): ");
                if (int.TryParse(Console.ReadLine(), out int j)) c_0 = j;
                Console.Write("Zvolte p_mut (def. 0.05): ");
                if (double.TryParse(Console.ReadLine(), out double y)) p_mut = y;

                while (true)
                {
                    Console.Clear();
                    Start_Climbing_Algorithm(t_max, c_0, p_mut);
                    Console.Write("Stiskněte 't' pro zvolení parametrů\nStiskněte 'k' pro ukončení\nNebo stiskněte cokoli pro opakování...");
                    ConsoleKey z = Console.ReadKey().Key;
                    if (z == ConsoleKey.K) return;
                    else if (z == ConsoleKey.T) break;
                }
            }
        }

        static void Start_Climbing_Algorithm(int t_max, int c_0, double p_mut)
        {
            double f_fin = int.MaxValue; //funkční hodnota a

            int n = 2; //počet jedinců
            int k = 3; //počet jedniček a nul

            string a_fin = GenerateA(n, k);

            for (int t = 1; t <= t_max; t++) //Hlavní cyklus
            {
                List<string> U = Mutation(a_fin, c_0, p_mut); //Rozpětí -> list zmutovaných 'a'

                foreach (string item in U) //Projíždí všechny zmutované 'a' -> hledá nejmenší funkční hodnotu
                {
                    List<int> lsA = new List<int>(); //List hodnot/parametrů

                    for (int j = 0; j < n * k; j += k)
                    {
                        string a_n = "";
                        for (int p = 0; p < k; p++) a_n += item[j + p];
                        lsA.Add(GrayToInt(a_n)); //List int hodnot
                    }
                    if (function1(lsA) < f_fin)
                    {
                        f_fin = function1(lsA);
                        a_fin = item;
                        Console.WriteLine("t: " + t);
                        Console.WriteLine($"U({U.IndexOf(item)}): {item}");
                        Console.WriteLine($"f1({lsA[0]},{lsA[1]}) = f_fin = {f_fin}");
                        Console.WriteLine("a_fin: " + a_fin);
                        Console.WriteLine("------------------");
                    }
                }
            }
        }

        static string GenerateA(int n, int k)
        {
            string a = "";
            for (int i = 0; i < n * k; i++) a += nc.Next(0, 2);
            return a;
        }

        static double function1(List<int> parameters)
        {
            int x = parameters[0];
            int y = parameters[1];

            return x * x + y * y;
        }

        static List<string> Mutation(string a, int c, double p)
        {
            List<string> U = new List<string>();
            
            for (int i = 0; i < c; i++) //vytváří 'c' zmutovaných 'a'
            {
                string a_new = "";

                for (int j = 0; j < a.Length; j++) //projíždí 'a' po číslicích/znacích
                {
                    if (nc.NextDouble() < p) //s pravděpodobností 'p' se znak změní
                    {
                        if (a[j] == '1') a_new += "0";
                        else a_new += "1";
                    }
                    else a_new += a[j]; //jinak bude znak stejný
                }

                U.Add(a_new);
            }

            return U;
        }

        static int GrayToInt(string a_n)
        {
            string binary = "";

            // MSB of binary code is same
            // as gray code
            binary += a_n[0];

            // Compute remaining bits
            for (int i = 1; i < a_n.Length; i++)
            {
                // If current bit is 0,
                // concatenate previous bit
                if (a_n[i] == '0') binary += binary[i - 1];

                // Else, concatenate invert of
                // previous bit
                else binary += (binary[i - 1] == '0' ? '1' : '0');
            }

            return Convert.ToInt32(binary, 2); ;
        }
    }
}
