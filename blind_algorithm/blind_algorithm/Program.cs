﻿using System;
using System.Collections.Generic;

namespace blind_algorithm
{
    class Program
    {
        static Random nc = new Random();

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();

                int t_max = 100;

                Console.Write("f(x,y) = x^2 + y^2\nZvolte t_max (def. 100): ");
                if (int.TryParse(Console.ReadLine(), out int i)) t_max = i;

                while (true)
                {
                    Console.Clear();
                    Start_Blind_Algorithm(t_max, 3, 2);
                    Console.Write("Stiskněte 't' pro zvolení parametrů\nStiskněte 'k' pro ukončení\nNebo stiskněte cokoli pro opakování...");
                    ConsoleKey z = Console.ReadKey().Key;
                    if (z == ConsoleKey.K) return;
                    else if (z == ConsoleKey.T) break;
                }
            }
            
        }

        static void Start_Blind_Algorithm(int t_max, int k, int n)
        {
            string a_fin = ""; //nejlepší potomek (a_min) -> hodnota na ose x v graycodu
            double f_fin = double.MaxValue; //funkční hodnota a

            for (int t = 1; t < t_max; t++)
            {
                string a = GenerateA(n, k);
                List<int> lsA = new List<int>();

                for (int j = 0; j < n*k; j+=k)
                {
                    string a_n = "";
                    for (int p = 0; p < k; p++) a_n += a[j+p];
                    lsA.Add(GrayToInt(a_n)); //List int hodnot 
                }

                if (function1(lsA) < f_fin)
                {
                    a_fin = a;
                    f_fin = function1(lsA);
                    Console.WriteLine("t: " + t);
                    Console.WriteLine($"f1({lsA[0]},{lsA[1]}) = f_fin = {f_fin}");
                    Console.WriteLine("a_fin: " + a_fin);
                    Console.WriteLine("------------------");
                }
            }
        }

        static double function1(List<int> parameters)
        {
            int x = parameters[0];
            int y = parameters[1];

            return x*x + y*y;
        }

        static string GenerateA(int n, int k)
        {
            string a = "";
            for (int i = 0; i < n*k; i++) a += nc.Next(0,2);
            return a;
        }

        static int GrayToInt(string a_n)
        {
            string binary = "";

            // MSB of binary code is same
            // as gray code
            binary += a_n[0];

            // Compute remaining bits
            for (int i = 1; i < a_n.Length; i++)
            {
                // If current bit is 0,
                // concatenate previous bit
                if (a_n[i] == '0') binary += binary[i-1];

                // Else, concatenate invert of
                // previous bit
                else binary += (binary[i-1] == '0' ? '1' : '0');
            }

            return Convert.ToInt32(binary, 2); ;
        }
    }
}
